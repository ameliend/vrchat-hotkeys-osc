# -*- coding: utf-8 -*-
"""A VRChat OSC software that allow you to create Hotkeys and send OSC signals to VRChat.

Using your keyboard to control your avatar parameters.
"""
import sys
import threading
import webbrowser
import winreg
from functools import partial
from getpass import getuser
from json import decoder, dump, dumps, load, loads
from os import getenv, remove, scandir
from pathlib import Path
from time import sleep

from PySide6 import QtGui, QtWidgets
from PySide6.QtCore import Qt
from pynput import keyboard
from pythonosc.udp_client import SimpleUDPClient

from vrchat_hotkeys_osc import darkstyle

RESOURCE_PATH = Path(__file__).resolve().parent.joinpath("resources")


class VrcHotkeysOscMainWindow(QtWidgets.QWidget):
    """The main window of the VRC Hotkeys OSC application.

    This window will read your VRChat OSC folder in your appdata and get your avatars parameters.
    You can add, or remove hotkeys to control your favorite avatars parameters,
    activate or deactivate the hotkeys listener.
    The hotkeys listener will then send message to VRChat trough OSC protocol.

    Attributes
    ----------
    config_file: Pathlib.Path
        The path of the .json config file used by the user.
    hotkeys: list
        The list of hotkeys loaded by the app, and used for the listener object.
    client: SimpleUDPClient
        The OSC client for VRChat on port 9000.
    listener: keyboard.GlobalHotKeys
        The input listener object.
    is_listener_running: bool
        The state of the listener thread.
    """

    DEFAULT_CONFIG_FILE = Path(getenv("APPDATA")).joinpath("VRChat Hotkey OSC", "config.json")

    def __init__(self):
        """Constructor for VRCHotkeysOSCMainWindow."""
        super().__init__()
        self.config_file = self.DEFAULT_CONFIG_FILE
        self.hotkeys = []
        self.client = SimpleUDPClient("127.0.0.1", 9000)
        self.listener = None
        self._is_listener_running = False
        self._init_ui()
        self._init_data()

    def _init_ui(self):
        self.setWindowFlag(Qt.Window)
        self.setWindowTitle("VRChat Hotkeys OSC")
        self.setWindowIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("logo.png"))))
        self.setFixedSize(600, 1000)
        # Menu bar.
        menu_bar = QtWidgets.QMenuBar(self)
        file_menu = menu_bar.addMenu("File")
        open_action = file_menu.addAction("Open Configuration...", self.load_configuration)
        open_action.setIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("open.png"))))
        save_action = file_menu.addAction("Save Configuration As...", self.save_configuration)
        save_action.setIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("save.png"))))
        edit_menu = menu_bar.addMenu("Edit")
        open_folder_action = edit_menu.addAction("Open OSC Avatars Folder", self._open_avatars_json_folder)
        open_folder_action.setIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("folder.png"))))
        refresh_action = edit_menu.addAction("Refresh Avatar List", self._init_data)
        refresh_action.setIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("refresh.png"))))
        purge_action = edit_menu.addAction("Purge Avatar List", self.purge_avatars_json_files)
        purge_action.setIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("delete2.png"))))
        more_menu = menu_bar.addMenu("More")
        open_web_action = more_menu.addAction("Open Gitlab Repository", self._open_repository)
        open_web_action.setIcon(QtGui.QIcon(str(RESOURCE_PATH.joinpath("web.png"))))
        # Main layout.
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.setSpacing(10)
        self.main_layout.setContentsMargins(30, 30, 30, 30)
        # Banner.
        banner = QtWidgets.QLabel(self)
        banner.setPixmap(QtGui.QPixmap(str(RESOURCE_PATH.joinpath("banner.png"))))
        banner.setAlignment(Qt.AlignCenter)
        # Avatar widgets.
        avatars_label = QtWidgets.QLabel("Avatar", self)
        self.avatars_name_combo = QtWidgets.QComboBox(self)
        self.avatars_name_combo.currentTextChanged.connect(self._on_avatars_name_combo_clicked)
        # Parameters widgets.
        parameters_label = QtWidgets.QLabel("Parameter", self)
        self.parameters_combo = QtWidgets.QComboBox(self)
        self.parameters_combo.currentTextChanged.connect(self._on_parameters_combo_clicked)
        # Action type widgets.
        action_type_label = QtWidgets.QLabel("Action type", self)
        self.action_type_combo = QtWidgets.QComboBox(self)
        self.action_type_combo.currentTextChanged.connect(self._on_action_type_combo_clicked)
        # Input delay widgets.
        input_delay = QtWidgets.QLabel("Input delay (in seconds)", self)
        self.input_delay_line_edit = QtWidgets.QLineEdit("0")
        # Single integer value widgets.
        single_val_label = QtWidgets.QLabel("Set single value", self)
        self.single_val_combo = QtWidgets.QComboBox()
        self.single_val_combo.addItems(["True", "False"])
        self.single_val_line_edit = QtWidgets.QLineEdit()
        self.single_val_line_edit.setVisible(False)
        # Double integer values widgets.
        double_val_label = QtWidgets.QLabel("Toggle between two values", self)
        double_val_widget = QtWidgets.QWidget()
        double_val_layout = QtWidgets.QHBoxLayout()
        double_val_layout.setContentsMargins(0, 5, 0, 0)
        double_val_widget.setLayout(double_val_layout)
        self.left_val_combo = QtWidgets.QComboBox()
        self.left_val_combo.addItems(["True", "False"])
        self.left_val_combo.currentTextChanged.connect(self._on_left_val_combo_clicked)
        self.left_val_line_edit = QtWidgets.QLineEdit()
        self.left_val_line_edit.setVisible(False)
        separator_label = QtWidgets.QLabel("And")
        self.right_val_combo = QtWidgets.QComboBox()
        self.right_val_combo.setEnabled(False)
        self.right_val_combo.addItem("False")
        self.right_val_line_edit = QtWidgets.QLineEdit()
        self.right_val_line_edit.setVisible(False)
        toggle_delay_label = QtWidgets.QLabel("Automatic delay (in seconds)", self)
        self.toggle_delay_line_edit = QtWidgets.QLineEdit()
        double_val_layout.addWidget(self.left_val_combo)
        double_val_layout.addWidget(self.left_val_line_edit)
        double_val_layout.addWidget(separator_label)
        double_val_layout.addWidget(self.right_val_combo)
        double_val_layout.addWidget(self.right_val_line_edit)
        double_val_layout.addWidget(toggle_delay_label)
        double_val_layout.addWidget(self.toggle_delay_line_edit)
        # Add hotkey button.
        add_hotkey_button = QtWidgets.QPushButton("Add Hotkey", self)
        add_hotkey_button.clicked.connect(self.add_hotkey)
        # Hotkey control widgets.
        hotkeys_control_groupbox = QtWidgets.QGroupBox(self)
        hotkeys_control_groupbox_layout = QtWidgets.QVBoxLayout(hotkeys_control_groupbox)
        hotkeys_control_groupbox_layout.setSpacing(4)
        self.hotkeys_control_treeview = QtWidgets.QTreeView()
        self.hotkeys_control_treeview.setAlternatingRowColors(True)
        header = QtWidgets.QHeaderView(Qt.Horizontal)
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        header.resizeSections()
        self.hotkeys_control_treeview.setHeader(header)
        self.hotkeys_control = QtGui.QStandardItemModel()
        self.hotkeys_control_treeview.setModel(self.hotkeys_control)
        self.hotkeys_control_treeview.selectionModel().selectionChanged.connect(self._context_menu_visibility)
        hotkeys_control_groupbox_layout.addWidget(self.hotkeys_control_treeview)
        # Activate button.
        self.toggle_activate_button = QtWidgets.QPushButton("Enable", self)
        self.toggle_activate_button.clicked.connect(self.toggle_thread)
        # Log widget.
        self.log_line_edit = QtWidgets.QLabel(self)
        self.log_line_edit.setStyleSheet("color: red")
        # Add widgets to main layout.
        self.main_layout.setMenuBar(menu_bar)
        self.main_layout.addWidget(banner)
        self.main_layout.addWidget(avatars_label)
        self.main_layout.addWidget(self.avatars_name_combo)
        self.main_layout.addWidget(parameters_label)
        self.main_layout.addWidget(self.parameters_combo)
        self.main_layout.addWidget(action_type_label)
        self.main_layout.addWidget(self.action_type_combo)
        self.main_layout.addWidget(input_delay)
        self.main_layout.addWidget(self.input_delay_line_edit)
        self.main_layout.addWidget(single_val_label)
        self.main_layout.addWidget(self.single_val_combo)
        self.main_layout.addWidget(self.single_val_line_edit)
        self.main_layout.addWidget(double_val_label)
        self.main_layout.addWidget(double_val_widget)
        self.main_layout.addWidget(add_hotkey_button)
        self.main_layout.addWidget(hotkeys_control_groupbox)
        self.main_layout.addWidget(self.toggle_activate_button)
        self.main_layout.addWidget(self.log_line_edit)
        # Context menu (delete hotkey).
        action = QtGui.QAction("Delete", self)
        action.setIcon(QtGui.QIcon(str(Path(__file__).resolve().parent / "resources" / "delete.png")))
        action.triggered.connect(self.remove_hotkey)
        self.hotkeys_control_treeview.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.hotkeys_control_treeview.addAction(action)
        self._context_menu_visibility()

    def _init_data(self):
        self.avatars_name_combo.clear()
        self.hotkeys_control.clear()
        self.hotkeys_control.setHorizontalHeaderLabels(
            ["Parameter", "Hotkey", "Input delay", "Type", "Value", "Toggle delay"]
        )
        vrc_osc_path = Path(rf"C:\Users\{getuser()}\AppData\LocalLow\VRChat\VRChat\OSC")
        if not vrc_osc_path.exists():
            self.log_line_edit.setText(
                "Error: VRChat OSC folder not detected. Please enable OSC feature in VRChat and try again."
            )
            return
        # Open the config file location registry key.
        try:
            with winreg.OpenKey(
                winreg.HKEY_CURRENT_USER,
                r"Software\Hotkeys OSC",
                access=winreg.KEY_READ | winreg.KEY_WRITE,
            ) as key:
                self.config_file = Path(winreg.QueryValueEx(key, "")[0])
        # If not found, creating a registry key to store the config file location.
        except FileNotFoundError:
            with winreg.CreateKey(winreg.HKEY_CURRENT_USER, r"Software\Hotkeys OSC") as key:
                winreg.SetValueEx(key, "", 0, winreg.REG_SZ, str(self.config_file))
        # Reading the configuration file and populating the hotkeys_control table view.
        if self.config_file.exists():
            try:
                with open(self.config_file, encoding="utf8") as file:
                    self.hotkeys = loads(load(file))
                    for hotkey in self.hotkeys:
                        row = []
                        for item in (
                            hotkey["parameter"],
                            hotkey["ui_hotkey"],
                            hotkey["ui_input_delay"],
                            hotkey["action_type"],
                            hotkey["ui_value"],
                            hotkey["ui_toggle_delay"],
                        ):
                            q_standard_item = QtGui.QStandardItem(item)
                            q_standard_item.setEditable(False)
                            row.append(q_standard_item)
                        self.hotkeys_control.appendRow(row)
            except (decoder.JSONDecodeError, PermissionError, AttributeError):
                self.log_line_edit.setText(
                    "Error: an error was encountered when reading the configuration file."
                )
                self.hotkeys = []
        # If not found, creating a default config file.
        else:
            if not self.DEFAULT_CONFIG_FILE.exists():
                self.DEFAULT_CONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
                self.DEFAULT_CONFIG_FILE.open("w", encoding="utf-8")
            # Creating a registry key to store the config file location.
            self.config_file = self.DEFAULT_CONFIG_FILE
            with winreg.CreateKey(winreg.HKEY_CURRENT_USER, r"Software\Hotkeys OSC") as key:
                winreg.SetValueEx(key, "", 0, winreg.REG_SZ, str(self.DEFAULT_CONFIG_FILE))
        # Reading the data from the VRChat OSC file.
        self.avatars_data = []
        for user in scandir(vrc_osc_path):
            for avatar_json_file in scandir(Path(user).joinpath("Avatars")):
                with open(avatar_json_file.path, encoding="utf-8-sig") as file:
                    self.avatars_data.append(load(file))
        # Adding avatars names to the combo box.
        for avatar_data in self.avatars_data:
            self.avatars_name_combo.addItem(avatar_data["name"])

    def _on_avatars_name_combo_clicked(self, avatar_name):
        self.parameters_combo.clear()
        avatar_data = next((data for data in self.avatars_data if data["name"] == avatar_name), None)
        if not avatar_data:
            return
        # Adding the parameters to the parameters combo box.
        for parameter in avatar_data["parameters"]:
            if parameter.get("input"):
                self.parameters_combo.addItem(parameter["name"])

    def _on_parameters_combo_clicked(self, parameter_name):
        self.action_type_combo.clear()
        avatar_name = self.avatars_name_combo.currentText()
        avatar_data = next((data for data in self.avatars_data if data["name"] == avatar_name), None)
        if not avatar_data:
            return
        # Adding the different types of actions that can be performed on the parameters.
        for parameter in avatar_data["parameters"]:
            if parameter["name"] == parameter_name:
                if parameter["input"]["type"] == "Bool":
                    self.action_type_combo.addItems(["Set Boolean", "Toggle Boolean"])
                elif parameter["input"]["type"] == "Float":
                    self.action_type_combo.addItems(["Set Float", "Toggle Float"])
                elif parameter["input"]["type"] == "Int":
                    self.action_type_combo.addItems(["Set Integer", "Toggle Integer"])

    def _on_action_type_combo_clicked(self, action_type):
        if action_type == "Set Boolean":
            self.single_val_combo.setVisible(True)
            self.single_val_combo.setEnabled(True)
            self.single_val_line_edit.setVisible(False)
            self.single_val_line_edit.setEnabled(False)
            self.left_val_combo.setVisible(True)
            self.left_val_combo.setEnabled(False)
            self.right_val_combo.setVisible(True)
            self.left_val_line_edit.setVisible(False)
            self.left_val_line_edit.setEnabled(False)
            self.right_val_line_edit.setVisible(False)
            self.right_val_line_edit.setEnabled(False)
            self.toggle_delay_line_edit.setEnabled(False)
        elif action_type == "Toggle Boolean":
            self.single_val_combo.setVisible(True)
            self.single_val_combo.setEnabled(False)
            self.single_val_line_edit.setVisible(False)
            self.single_val_line_edit.setEnabled(False)
            self.left_val_combo.setVisible(True)
            self.left_val_combo.setEnabled(True)
            self.right_val_combo.setVisible(True)
            self.left_val_line_edit.setVisible(False)
            self.left_val_line_edit.setEnabled(False)
            self.right_val_line_edit.setVisible(False)
            self.right_val_line_edit.setEnabled(False)
            self.toggle_delay_line_edit.setEnabled(True)
        elif action_type in ["Set Float", "Set Integer"]:
            self.single_val_combo.setVisible(False)
            self.single_val_combo.setEnabled(False)
            self.single_val_line_edit.setVisible(True)
            self.single_val_line_edit.setEnabled(True)
            self.left_val_combo.setVisible(False)
            self.left_val_combo.setEnabled(False)
            self.right_val_combo.setVisible(False)
            self.left_val_line_edit.setVisible(True)
            self.left_val_line_edit.setEnabled(False)
            self.right_val_line_edit.setVisible(True)
            self.right_val_line_edit.setEnabled(False)
            self.toggle_delay_line_edit.setEnabled(False)
        elif action_type in ["Toggle Float", "Toggle Integer"]:
            self.single_val_combo.setVisible(False)
            self.single_val_combo.setEnabled(False)
            self.single_val_line_edit.setVisible(True)
            self.single_val_line_edit.setEnabled(False)
            self.left_val_combo.setVisible(False)
            self.left_val_combo.setEnabled(False)
            self.right_val_combo.setVisible(False)
            self.left_val_line_edit.setVisible(True)
            self.left_val_line_edit.setEnabled(True)
            self.right_val_line_edit.setVisible(True)
            self.right_val_line_edit.setEnabled(True)
            self.toggle_delay_line_edit.setEnabled(True)

    def _on_left_val_combo_clicked(self, combo_value):
        self.right_val_combo.clear()
        if combo_value == "True":
            self.right_val_combo.addItem("False")
            return
        self.right_val_combo.addItem("True")

    def _context_menu_visibility(self):
        for action in self.hotkeys_control_treeview.actions():
            try:
                _ = self.hotkeys_control_treeview.selectionModel().selectedIndexes()[0]
                action.setVisible(True)
            except IndexError:
                action.setVisible(False)

    @staticmethod
    def _open_repository():
        webbrowser.open("https://gitlab.com/ameliend/vrchat-hotkey-osc")

    @staticmethod
    def _open_avatars_json_folder():
        webbrowser.open(rf"C:\Users\{getuser()}\AppData\LocalLow\VRChat\VRChat\OSC")

    def save_configuration(self):
        """Open a file dialog window and save the current configuration to a new filepath.

        Note
        ----
        The registry Hotkeys OSC key will be overwritten with the new filepath.
        So new path is loaded when Hotkeys OSC is lunched again.
        """
        # Creating a file dialog box that allows the user to select a file to save.
        result, _ = QtWidgets.QFileDialog.getSaveFileName(
            self, "Save Configuration", str(self.config_file), "Json file (*.json)"
        )
        if not result:
            return
        self.config_file = Path(result).resolve()
        if not self.config_file.exists():
            self.config_file.open("w", encoding="utf-8")
        # Append data to the new config filepath.
        with open(self.config_file, "w", encoding="utf8") as file:
            dump(dumps(self.hotkeys), file)
        # Write new config filepath to registry key.
        try:
            key = winreg.OpenKey(
                winreg.HKEY_CURRENT_USER,
                r"Software\Hotkeys OSC",
                access=winreg.KEY_READ | winreg.KEY_WRITE,
            )
        # If not found, create and write new config filepath.
        except FileNotFoundError:
            key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, r"Software\Hotkeys OSC")
        winreg.SetValueEx(key, "", 0, winreg.REG_SZ, str(self.config_file))
        winreg.CloseKey(key)
        # Clear errors log (if any).
        self.log_line_edit.clear()

    def load_configuration(self):
        """Open a file dialog window and load a given configuration filepath.

        Note
        ----
        The registry Hotkeys OSC key will be overwritten with the new filepath.
        So new path is loaded when Hotkeys OSC is lunched again.
        """
        # Disable the listener thread before removing a hotkey.
        if self._is_listener_running:
            self._is_listener_running = False
            self.toggle_activate_button.setText("Enable")
            self.listener.stop()
        # Creating a file dialog box that allows the user to select a file.
        result, _ = QtWidgets.QFileDialog.getOpenFileName(
            self, "Save Configuration", str(self.config_file), "Json file (*.json)"
        )
        if not result:
            return
        self.config_file = Path(result).resolve()
        # Write new config filepath to registry key.
        try:
            key = winreg.OpenKey(
                winreg.HKEY_CURRENT_USER,
                r"Software\Hotkeys OSC",
                access=winreg.KEY_READ | winreg.KEY_WRITE,
            )
        # If not found, create and write new config filepath.
        except FileNotFoundError:
            key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, r"Software\Hotkeys OSC")
        winreg.SetValueEx(key, "", 0, winreg.REG_SZ, str(self.config_file))
        winreg.CloseKey(key)
        # Re-init data.
        self._init_data()
        # Clear errors log (if any).
        self.log_line_edit.clear()

    def add_hotkey(self):
        """Add a new hotkey to the hotkeys list and appends it to the config file."""
        if not (parameter := self.parameters_combo.currentText()):
            return
        action_type = self.action_type_combo.currentText()
        input_delay = self.input_delay_line_edit.text().replace(",", ".")
        if input_delay.isalpha():
            self.log_line_edit.setText("Error: The input delay value must be digits only.")
            return
        if input_delay == "0" or not input_delay:
            input_delay = 0
            ui_input_delay = "None"
        elif float(input_delay) < 0.1:
            input_delay = 0.1
            ui_input_delay = "0.1 second(s)"
        else:
            input_delay = float(input_delay)
            ui_input_delay = f"{input_delay} second(s)"
        if self.single_val_combo.isEnabled():
            value = self.single_val_combo.currentText() == "True"
            ui_value = str(value)
        elif self.single_val_line_edit.isEnabled():
            ui_value = self.single_val_line_edit.text()
            if ui_value.isalpha():
                self.log_line_edit.setText("Error: The value must be digits only.")
                return
            value = float(ui_value)
        elif self.left_val_combo.isEnabled():
            value = self.left_val_combo.currentText() == "True"
            if value:
                value = [True, False]
                ui_value = "True/False"
            else:
                value = [False, True]
                ui_value = "False/True"
        else:
            left_value = self.left_val_line_edit.text()
            right_value = self.right_val_line_edit.text()
            if left_value.isalpha() or right_value.isalpha():
                self.log_line_edit.setText("Error: The value must be digits only.")
                return
            value = [float(left_value), float(right_value)]
            ui_value = f"{left_value}/{right_value}"
        toggle_delay = self.toggle_delay_line_edit.text().replace(",", ".")
        if isinstance(value, list):
            if toggle_delay.isalpha():
                self.log_line_edit.setText("Error: The toggle delay value must be digits only.")
                return
            if toggle_delay == "0" or not toggle_delay:
                toggle_delay = 0
                ui_toggle_delay = "None"
            elif float(toggle_delay) < 0.1:
                toggle_delay = 0.1
                ui_toggle_delay = "0.1 second(s)"
            else:
                toggle_delay = float(toggle_delay)
                ui_toggle_delay = f"{toggle_delay} second(s)"
        else:
            toggle_delay = 0
            ui_toggle_delay = "None"
        # Disable the listener thread before adding a new hotkey.
        if self._is_listener_running:
            self._is_listener_running = False
            self.toggle_activate_button.setText("Enable")
            self.listener.stop()
        # Creating a dialog box that will allow the user to capture a keystroke combination.
        key_capture_window = HotkeyCreatorDialog(self)
        key_capture_window.exec()
        # If the hotkey capture get simple key.
        if key_capture_window.captured_key and not key_capture_window.captured_special_keys:
            hotkey = key_capture_window.captured_key
            ui_hotkey = key_capture_window.captured_key.upper()
        elif key_capture_window.captured_key:
            hotkey = (
                "+".join(key_capture_window.captured_special_keys).lower()
                + f"+{key_capture_window.captured_key}"
            )
            ui_hotkey = (
                f"{' + '.join(key_capture_window.captured_special_keys).upper()}"
                f" + {key_capture_window.captured_key.upper()}"
            )
        else:
            return
        self.hotkeys.append(
            {
                "parameter": parameter,
                "action_type": action_type,
                "input_delay": input_delay,
                "ui_input_delay": ui_input_delay,
                "value": value,
                "ui_value": ui_value,
                "toggle_delay": toggle_delay,
                "ui_toggle_delay": ui_toggle_delay,
                "hotkey": hotkey,
                "ui_hotkey": ui_hotkey,
            }
        )
        # Append data to the config file.
        with open(self.config_file, "w", encoding="utf8") as file:
            dump(dumps(self.hotkeys), file)
        # Adding a row to the hotkeys_control QTableView.
        row = []
        for item in (
            parameter,
            ui_hotkey,
            ui_input_delay,
            action_type,
            ui_value,
            ui_toggle_delay,
        ):
            q_standard_item = QtGui.QStandardItem(item)
            q_standard_item.setEditable(False)
            row.append(q_standard_item)
        self.hotkeys_control.appendRow(row)
        # Clear errors log (if any).
        self.log_line_edit.clear()

    def remove_hotkey(self):
        """Removes the hotkey from the list of hotkeys, and from the treeview."""
        if self._is_listener_running:
            self._is_listener_running = False
            self.toggle_activate_button.setText("Enable")
            self.listener.stop()
        index = self.hotkeys_control_treeview.currentIndex().row()
        self.hotkeys.pop(index)
        with open(self.config_file, "w", encoding="utf8") as file:
            dump(dumps(self.hotkeys), file)
        self.hotkeys_control.takeRow(index)

    def purge_avatars_json_files(self):
        """Purge all the .json files in the OSC folder of VRChat."""
        vrc_osc_path = Path(rf"C:\Users\{getuser()}\AppData\LocalLow\VRChat\VRChat\OSC")
        if not vrc_osc_path.exists():
            self.log_line_edit.setText(
                "Error: VRChat OSC folder not detected. Please enable OSC feature in VRChat and try again."
            )
            return
        # Removing all the avatar files from the vrc_osc folder.
        for user in scandir(vrc_osc_path):
            for avatar_json_file in scandir(Path(user) / "Avatars"):
                remove(avatar_json_file.path)
        # Re-init data.
        self._init_data()
        # Clear errors log (if any).
        self.log_line_edit.clear()

    def send_osc(self, hotkey):
        """Send an OSC message to the avatar with a delay if the hotkey has one.

        Parameters
        ----------
        hotkey: dict
                the hotkey that was pressed
        """
        input_delay = hotkey["input_delay"]
        toggle_delay = hotkey["toggle_delay"]
        parameter = hotkey["parameter"]
        value = hotkey["value"]
        if isinstance(value, list):
            if input_delay > 0:
                timer = threading.Timer(
                    input_delay,
                    partial(self.client.send_message, f"/avatar/parameters/" f"{parameter}", value[0]),
                )
                timer.start()
            else:
                self.client.send_message(f"/avatar/parameters/" f"{parameter}", value[0])
            if toggle_delay > 0:
                timer = threading.Timer(
                    input_delay + toggle_delay,
                    partial(self.client.send_message, f"/avatar/parameters/" f"{parameter}", value[1]),
                )
                timer.start()
            else:
                value.reverse()
        elif input_delay > 0:
            timer = threading.Timer(
                input_delay,
                partial(self.client.send_message, f"/avatar/parameters/" f"{parameter}", value),
            )
            timer.start()
        else:
            self.client.send_message(f"/avatar/parameters/" f"{parameter}", value)

    def toggle_thread(self):
        """Start the listener thread that listens for those hotkeys."""

        def generate_listener_dict():
            pyinput_dict = {}
            for hotkey in self.hotkeys:
                # # If hotkey is alt, also create an alt_gr.
                # if "<alt>" in hotkey_dict["hotkey"]:
                #     alt_gr = hotkey_dict["hotkey"].replace("<alt>", "<alt_gr>")
                #     pyinput_dict[alt_gr] = partial(self.send_osc, hotkey_id)
                pyinput_dict[hotkey["hotkey"]] = partial(self.send_osc, hotkey)
            return {key: pyinput_dict[key] for key in sorted(pyinput_dict, key=len, reverse=True)}

        if not self._is_listener_running:
            self._is_listener_running = True
            self.toggle_activate_button.setText("Disable")
            self.listener = keyboard.GlobalHotKeys(generate_listener_dict())
            self.listener.start()
        else:
            self._is_listener_running = False
            self.toggle_activate_button.setText("Enable")
            self.listener.stop()


class HotkeyCreatorDialog(QtWidgets.QDialog):
    """A dialog popup to listen the key combinations from the VRCHotkeysOSCMainWindow.

    Parameters
    ----------
    parent : VRCHotkeysOSCMainWindow
        The parent widget.

    Attributes
    ----------
    captured_key : str
        The simple captured key.
    captured_special_keys : list
        A list of special captured key combination's
    listener : keyboard.Listener
        The listener object.
    """

    def __init__(self, parent):
        """Constructor for HotkeyCreatorDialog."""
        super().__init__(parent=parent)
        self.captured_key = None
        self.captured_special_keys = []
        self.listener = keyboard.Listener(on_press=self._on_press, on_release=self._on_release)
        self.listener.start()
        self._init_ui()

    def _init_ui(self):
        self.setFixedSize(200, 50)
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.label = QtWidgets.QLabel("Waiting for input...", self)
        self.main_layout.addWidget(self.label)
        self.main_layout.setAlignment(Qt.AlignCenter)

    def _on_press(self, key):
        def stop_listener():
            sleep(0.01)
            self.listener.stop()
            self.done(0)

        # Try to capture a simple key.
        try:
            self.captured_key = key.char
            stop_listener()
        # Else, keys are key combinations.
        except AttributeError:
            # Simplify the key pressed name.
            key = f'{str(key).upper().split("KEY.")[1].split("_")[0]}'
            # These are for Function keys (F1, F2, F3...).
            if key.startswith("F"):
                self.captured_key = f"<{key}>"
                stop_listener()
                return
            if key == "SPACE":
                self.captured_key = key
                stop_listener()
                return
            if key == "CTRL":
                # The controller need to release manually the key pressed,
                # in order to fix return a ctrl char (CTRL + F return ^F).
                controller = keyboard.Controller()
                controller.release(keyboard.Key.ctrl_l)
                controller.release(keyboard.Key.ctrl_r)
            # If the special key is alone.
            if not self.captured_special_keys:
                self.label.setText(f"<{key}> + ")
                self.captured_special_keys = [f"<{key}>"]
            # If multiple special keys.
            elif f"<{key}>" not in self.captured_special_keys:
                keys = " + ".join(self.captured_special_keys)
                self.label.setText(f"{keys} + <{key}> +")
                self.captured_special_keys.append(f"<{key}>")

    def _on_release(self, key):
        # Simplify the key pressed name.
        if f"{str(key).upper().split('KEY.')[1].split('_')[0]}" != "CTRL":
            # If the key is CTRL,
            # the controller need to release manually the key pressed,
            # in order to fix return a ctrl char (CTRL + F return ^F)
            self.captured_special_keys.clear()

    def closeEvent(self, _):
        """Override the close event to stop the listener thread."""
        self.listener.stop()


if __name__ == "__main__":
    app = QtWidgets.QApplication()
    darkstyle.apply(app)
    vrc_hotkeys_osc_main_window = VrcHotkeysOscMainWindow()
    vrc_hotkeys_osc_main_window.show()
    sys.exit(app.exec())
